package com.example.multiapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class resultados_salud extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultados_salud);

        final TextView tvContenido = findViewById(R.id.tvContenido);
        tvContenido.setText(getIntent().getStringExtra("recomendacion"));
        final TextView tvTitulo = findViewById(R.id.tvTitulo);
        tvTitulo.setText(getIntent().getStringExtra("titulo"));
    }
}