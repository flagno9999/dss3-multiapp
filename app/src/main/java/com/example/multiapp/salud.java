package com.example.multiapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;
import android.view.View;

public class salud extends AppCompatActivity {

    EditText etNombre, etPeso, etAltura;
    Button btnCalcular, btnLimpiar;
    RadioButton rbVaron, rbMujer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salud);

        etNombre = findViewById(R.id.txtNombre);
        etPeso = findViewById(R.id.txtPeso);
        etAltura = findViewById(R.id.txtAltura);
        btnCalcular = findViewById(R.id.bCalcular);
        btnLimpiar = findViewById(R.id.bLimpiar);
        rbVaron = findViewById(R.id.rHombre);
        rbMujer = findViewById(R.id.rMujer);

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Limpiar();
            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalcularIMC();
            }
        });
    }

    private void Limpiar() {
        etNombre.setText("");
        etAltura.setText("");
        etPeso.setText("");
    }

    private void MensajeIMC(double IMC) {
        String mensaje = "";
        Integer grado = 0;
        if (IMC < 16) {
            mensaje = "Bajo peso muy grave";
            grado = 1;
        } else if (IMC >= 16 & IMC < 17) {
            mensaje = "Bajo peso grave";
            grado = 1;
        } else if (IMC >= 17 & IMC < 18.5) {
            mensaje = "Bajo peso";
            grado = 1;
        } else if (IMC >= 18.5 & IMC < 25) {
            mensaje = "Peso normal";
            grado = 2;
        } else if (IMC >= 25 & IMC < 30) {
            mensaje = "Sobrepeso";
            grado = 3;
        } else if (IMC >= 30 & IMC < 35) {
            mensaje = "Obesidad grado I";
            grado = 3;
        } else if (IMC >= 35 & IMC < 40) {
            mensaje = "Obesidad grado II";
            grado = 3;
        } else {
            mensaje = "Obesidad grado III";
            grado = 3;
        }

        Toast.makeText(this, "Tu IMC es" + String.format("%.2f", IMC) + mensaje, Toast.LENGTH_LONG).show();
        String recomendacion = "";
        Intent intent = new Intent(this, resultados_salud.class);
        switch (grado) {
            case 1:
                recomendacion = getString(R.string.imc_grado_1);
                intent.putExtra("recomendacion", recomendacion);
                intent.putExtra("titulo", mensaje);
                startActivity(intent);
                break;
            case 2:
                recomendacion = getString(R.string.imc_grado_2);
                intent.putExtra("recomendacion", recomendacion);
                intent.putExtra("titulo", mensaje);
                startActivity(intent);
                break;
            case 3:
                recomendacion = getString(R.string.imc_grado_3);
                intent.putExtra("recomendacion", recomendacion);
                intent.putExtra("titulo", mensaje);
                startActivity(intent);
                break;
        }
    }

    private void CalcularIMC() {
        if (validarDigitos()) {
            String nombre = etNombre.getText().toString();
            Double altura = Double.valueOf(etAltura.getText().toString());
            Double peso = Double.valueOf(etPeso.getText().toString());
            double IMC = peso / Math.pow(altura, 2);
            MensajeIMC(IMC);
        } else {
            Toast.makeText(this, "Llene todos los campos", Toast.LENGTH_SHORT).show();
        }


    }

    public boolean validarDigitos() {
        if (etAltura.getText().length() > 0 && etPeso.getText().length() > 0 && etNombre.getText().length() > 0) {
            return true;
        } else {
            return false;
        }
    }
}

