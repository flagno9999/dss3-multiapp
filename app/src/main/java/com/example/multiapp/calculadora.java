package com.example.multiapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class calculadora extends AppCompatActivity {

    EditText et1;
    EditText et2;
    EditText etResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        et1 = findViewById(R.id.et1);
        et2 = findViewById(R.id.et2);
        etResult =  findViewById(R.id.etResult);
    }

    public void sumar(View view){
    if(validarDigitos()){
        etResult.setText( ""+ (Double.parseDouble(et1.getText().toString() ) + Double.parseDouble(et2.getText().toString() ))  );
    }else{
        Toast.makeText(this,"Llene ambos campos para esta operación",Toast.LENGTH_LONG).show();
    }

    }

    public void restar(View view){

        if(validarDigitos()){
            etResult.setText( ""+ (Double.parseDouble(et1.getText().toString() ) - Double.parseDouble(et2.getText().toString() ))  );
        }else{
            Toast.makeText(this,"Llene ambos campos para esta operación",Toast.LENGTH_LONG).show();
        }
    }

    public void calcular(View view){

        if(validarDigitos()){
            etResult.setText( ""+ (Double.parseDouble(et1.getText().toString() ) * Double.parseDouble(et2.getText().toString() ))  );
        }else{
            Toast.makeText(this,"Llene ambos campos para esta operación",Toast.LENGTH_LONG).show();
        }
    }

    public void dividir(View view){
        if(et2.getText().equals("0")){
            Toast.makeText(this,"El segundo numero no puede ser 0",Toast.LENGTH_LONG).show();
        }else{
            if(validarDigitos()){
                etResult.setText( ""+ (Double.parseDouble(et1.getText().toString() ) / Double.parseDouble(et2.getText().toString() ))  );
            }else{
                Toast.makeText(this,"Llene ambos campos para esta operación",Toast.LENGTH_LONG).show();
            }
        }

    }

    public void potencia(View view){

        if(validarDigitos()){
            etResult.setText( ""+ (Math.pow(Double.parseDouble(et1.getText().toString() ) , Double.parseDouble(et2.getText().toString() )))  );
        }else{
            Toast.makeText(this,"Llene ambos campos para esta operación",Toast.LENGTH_LONG).show();
        }
    }

    public void factorial(View view){
        etResult.setText( ""+ (calculoFactorial(Double.parseDouble(et1.getText().toString() )))  );

    }

    public void seno(View view){
        etResult.setText( ""+ (Math.sin(Double.parseDouble(et1.getText().toString() )))  );
    }

    public void tangente(View view){
        etResult.setText( ""+ (Math.tan(Double.parseDouble(et1.getText().toString() )))  );
    }

    public void limpiar(View view){
         et1.setText("");
         et2.setText("");
         etResult.setText("");
    }

    public double calculoFactorial (double numero) {
        if (numero==0)
            return 1;
        else
            return numero * calculoFactorial(numero-1);
    }

    public boolean validarDigitos(){
        if(et1.getText().length()>0 && et2.getText().length()>0){
            return true;
        }else{
            return false;
        }
    }
}