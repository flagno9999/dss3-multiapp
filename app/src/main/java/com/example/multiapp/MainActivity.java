package com.example.multiapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void actividad1(View view) {
        Intent intent = new Intent(this, calculadora.class);
        startActivity(intent);
    }

    public void actividad2(View view) {
        Intent intent = new Intent(this, salud.class);
        startActivity(intent);
    }

    public void salir(View view) {
    finish();
    }
}